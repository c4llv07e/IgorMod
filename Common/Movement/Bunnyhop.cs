using Terraria;
using Terraria.ModLoader;

namespace IgorMod.Common.Movement
{
    public sealed class Bunnyhop : ModPlayer {
        public static float DefaultBoost => 0.8f;

        public uint NumTicksOnGround { get; set; }
        public float Boost { get; set; }

        public override void ResetEffects()
        {
            Boost = DefaultBoost;
        }

        public override bool PreItemCheck()
        {

            bool onGround = Player.velocity.Y == 0f;
            bool wasOnGround = Player.oldVelocity.Y == 0f;
            
            if (!onGround && wasOnGround && NumTicksOnGround < 3) {
                float boostAdd = 0f;
                float boostMultiplier = 1f;

                //IPlayerOnBunnyhopHook.Invoke(Player, ref boostAdd, ref boostMultiplier);

                float totalBoost = (Boost + boostAdd) * boostMultiplier;

                Player.velocity.X += ((Player.controlRight ? 1f : 0f) - (Player.controlLeft ? 1f : 0f)) * totalBoost;
            }

            if (onGround) {
                NumTicksOnGround++;
            } else {
                NumTicksOnGround = 0;
            }

            return base.PreItemCheck();
        }
    }
}