using Terraria;
using Terraria.ModLoader;

namespace IgorMod.Content.Items
{
    public abstract class AbstractItem : ModItem
    {
        public override string Texture => "IgorMod/Content/All/Unloaded";
    }
}