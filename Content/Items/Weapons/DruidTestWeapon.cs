using Terraria;
using Terraria.ModLoader;
using Terraria.ID;
using System;

namespace IgorMod.Content.Items.Weapons
{
    public class DruidTestWeapon : DruidAbstractWeapon
    {
        public override void SetDefaults()
        {
            base.SetDefaults();
            Item.width = 40;
			Item.height = 40;
			Item.useStyle = ItemUseStyleID.Swing;
			Item.useTime = 30;
			Item.useAnimation = 30;
			Item.autoReuse = true;
			Item.damage = 70;
			Item.knockBack = 4;
			Item.crit = 6;
			Item.value = Item.buyPrice(gold: 1);
			Item.rare = ItemRarityID.Green;
			Item.UseSound = SoundID.Item1;
        }
		public override void AddRecipes() 
		{
			CreateRecipe()
				.AddIngredient(ItemID.DirtBlock, 20)
				.AddTile(TileID.WorkBenches)
				.Register();
		}
    }
}