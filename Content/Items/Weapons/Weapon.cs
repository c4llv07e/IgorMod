using Terraria;
using Terraria.ModLoader;
using Terraria.ID;
using System;
using System.Collections.Generic;
using System.Linq;
using IgorMod.Content.Projectiles;

namespace IgorMod.Content.Items.Weapons
{
    public class Weapon : DruidAbstractWeapon
    {
        public override void SetDefaults()
        {
            base.SetDefaults();
            Item.damage = 25;
			Item.width = 12;
			Item.height = 12;
			Item.useTime = 4;
			Item.useAnimation = 8;
			Item.useStyle = ItemUseStyleID.Shoot;
			Item.noMelee = true;
			Item.knockBack = 6;
			Item.value = 10000;
			Item.rare = ItemRarityID.LightRed;
			Item.UseSound = SoundID.Grass;
			Item.autoReuse = true;
			Item.shoot = ModContent.ProjectileType<Projectiles.DruidTestProjectile>();
			Item.shootSpeed = 20;
			Item.crit = 32;
			Item.mana = 1;
        }

        public override void AddRecipes() {
			CreateRecipe()
				.AddIngredient(ItemID.DirtBlock, 20)
				.AddTile(TileID.WorkBenches)
				.Register();
		}
    }
}