using IgorMod.Content.DamageClasses;
using Terraria;
using Terraria.GameContent.Creative;
using Terraria.ID;
using Terraria.ModLoader;
using System.Collections.Generic;
using System.Linq;

namespace IgorMod.Content.Items.Weapons
{
	public abstract class DruidAbstractWeapon : AbstractItem
	{

		public override void SetDefaults() 
		{
			Item.DamageType = ModContent.GetInstance<DruidDamageClass>();
		}

		public override void ModifyTooltips(List<TooltipLine> tooltips)
		{
			var tt = tooltips.FirstOrDefault(x => x.Name == "Damage");
			if (tt == null) return;
			string[] lines = tt.Text.Split(' ');
			tt.Text = lines.First() + " druid damage";
		}
	}
}