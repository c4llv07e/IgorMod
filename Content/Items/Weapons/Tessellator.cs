using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace IgorMod.Content.Items.Weapons
{
	public class Tessellator : ModItem
	{
		public override string Texture => "IgorMod/Content/Items/Weapons/Tessellator";
		public override void SetStaticDefaults()
		{
			DisplayName.SetDefault("Tessellator");
			Tooltip.SetDefault("This is a glorious Igor's sword.");
		}

		public override void SetDefaults()
		{
			Item.damage = 150;
			Item.DamageType = DamageClass.Melee;
			Item.width = 40;
			Item.height = 40;
			Item.useTime = 20;
			Item.useAnimation = 10;
			Item.useStyle = 1;
			Item.knockBack = 6;
			Item.value = 10000;
			Item.rare = ItemRarityID.Red;
			Item.UseSound = SoundID.Item1;
			Item.autoReuse = true;
		}

		public override void AddRecipes()
		{
			CreateRecipe()
				.AddIngredient(ItemID.DirtBlock, 10)
				.AddTile(TileID.WorkBenches)
				.Register();
		}
	}
}