using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using IgorMod.Common.Players;

namespace IgorMod.Content.Items.Armor
{
	public class DruidTestArmor : ModItem
	{
		public override void Load()
        {
			if (Main.netMode == NetmodeID.Server)
				return;

			// Add equip textures
			EquipLoader.AddEquipTexture(Mod, $"{Texture}_{EquipType.Head}", EquipType.Head, this, equipTexture: new DruidTestHead());
			EquipLoader.AddEquipTexture(Mod, $"{Texture}_{EquipType.Body}", EquipType.Body, this);
			EquipLoader.AddEquipTexture(Mod, $"{Texture}_{EquipType.Legs}", EquipType.Legs, this);

			//Add a separate set of equip textures by providing a custom name reference instead of an item reference
			EquipLoader.AddEquipTexture(Mod, $"{Texture}Alt_{EquipType.Head}", EquipType.Head, name: "DruidTestArmorAlt", equipTexture: new DruidTestHead());
			EquipLoader.AddEquipTexture(Mod, $"{Texture}Alt_{EquipType.Body}", EquipType.Body, name: "DruidTestArmorAlt");
			EquipLoader.AddEquipTexture(Mod, $"{Texture}Alt_{EquipType.Legs}", EquipType.Legs, name: "DruidTestArmorAlt");
		}

		private void SetupDrawing()
        {
			// Since the equipment textures weren't loaded on the server, we can't have this code running server-side
			if (Main.netMode == NetmodeID.Server)
				return;

			int equipSlotHead = EquipLoader.GetEquipSlot(Mod, Name, EquipType.Head);
			int equipSlotBody = EquipLoader.GetEquipSlot(Mod, Name, EquipType.Body);
			int equipSlotLegs = EquipLoader.GetEquipSlot(Mod, Name, EquipType.Legs);

			int equipSlotHeadAlt = EquipLoader.GetEquipSlot(Mod, "DruidTestArmorAlt", EquipType.Head);
			int equipSlotBodyAlt = EquipLoader.GetEquipSlot(Mod, "DruidTestArmorAlt", EquipType.Body);
			int equipSlotLegsAlt = EquipLoader.GetEquipSlot(Mod, "DruidTestArmorAlt", EquipType.Legs);

			ArmorIDs.Head.Sets.DrawHead[equipSlotHead] = false;
			ArmorIDs.Head.Sets.DrawHead[equipSlotHeadAlt] = false;
			ArmorIDs.Body.Sets.HidesTopSkin[equipSlotBody] = true;
			ArmorIDs.Body.Sets.HidesArms[equipSlotBody] = true;
			ArmorIDs.Body.Sets.HidesTopSkin[equipSlotBodyAlt] = true;
			ArmorIDs.Body.Sets.HidesArms[equipSlotBodyAlt] = true;
			ArmorIDs.Legs.Sets.HidesBottomSkin[equipSlotLegs] = true;
			ArmorIDs.Legs.Sets.HidesBottomSkin[equipSlotLegsAlt] = true;
		}

		public override void SetStaticDefaults()
        {
			DisplayName.SetDefault("Charm of Example");
			Tooltip.SetDefault("Turns the holder into Blocky near town NPC"
				+ "\nBlocky's colors will invert in water");

			SetupDrawing();
		}

		public override void SetDefaults()
        {
			Item.width = 24;
			Item.height = 28;
			Item.accessory = true;
			Item.value = Item.buyPrice(gold: 15);
			Item.rare = ItemRarityID.Pink;
			Item.canBePlacedInVanityRegardlessOfConditions = true;
		}

		public override void UpdateAccessory(Player player, bool hideVisual)
        {
			var p = player.GetModPlayer<DruidTestArmorPlayer>();
			p.DruidAccessory = true;
			p.DruidHideVanity = hideVisual;
		}

		public override bool IsVanitySet(int head, int body, int legs) => true;
	}

	public class DruidTestHead : EquipTexture
	{
		public override void UpdateVanitySet(Player player)
        {
			if (Main.rand.NextBool(20)) {
				Dust.NewDust(player.position, player.width, player.height, DustID.Blood);
			}
		}
	}
}